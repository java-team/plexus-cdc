#!/bin/sh -e

# There is no svn tag for plexus-cdc! :-(

VERSION=1.0-alpha-14
REVISION=7576

DEBVERSION=$(echo $VERSION | sed -e's,-alpha-,~alpha,')

TAR=../plexus-cdc_$DEBVERSION.orig.tar.gz
DIR=plexus-cdc-$VERSION

svn export -r$REVISION http://svn.codehaus.org/plexus/archive/plexus-cdc $DIR
tar -c -z -f $TAR $DIR
rm -rf $DIR
